import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
from xgboost import XGBClassifier
from sklearn.model_selection import train_test_split
from xgboost import XGBClassifier
import matplotlib.pyplot as plt
import seaborn as sns
sns.set(style="ticks", color_codes=True)


test_data = pd.read_csv('Dataset/test.csv')
train_data = pd.read_csv('Dataset/train.csv')
 

train_test_data = [train_data, test_data] #combining test and train data sets
#creating a new column called 'Title' from the Name column & removing all alphabets that isn't a title
for dataset in train_test_data:
    dataset['Title']= dataset['Name'].str.extract ('([A-Za-z]+)\.', expand = False)
    
#Title mapping to numbers: I am only going to use mr,mrs,miss

title_mapping = {'Mr':0,'Miss':1, 'Mrs':2, 'Master':3,'Dr':3,'Rev':3, 'Col':3,'Major':3, 'Mlle':3,'Lady':3,'Capt':3,'Sir':3,'Don':3,'Countess':3,'Mme':3,'JohnKheer':3,'Ms':3}

for dataset in train_test_data:
    dataset['Title']= dataset['Title'].map(title_mapping)
    
#Because some values of the 'Age' are missing ,I will fill in the values using the median age of each 'Title'(Mr, Mrs, Miss, Others) and 'Age'

train_data['Age'].fillna(train_data.groupby ('Title')['Age'].transform('median'), inplace= True)
test_data['Age'].fillna(test_data.groupby ('Title')['Age'].transform('median'), inplace= True)

#Sex mapping for female = 0 , male =1
sex_mapping = {'female':0, 'male':1}
for dataset in train_test_data:
    dataset['Sex']= dataset ['Sex'].map(sex_mapping)
    
#Combining Sibling and parent count into one column called FamilySize
train_data['FamilySize'] = train_data['SibSp'] + train_data['Parch']+1
test_data['FamilySize'] = test_data['SibSp'] + test_data['Parch']+1

family_mapping = {1:0, 2:0.1, 3:0.2, 4:0.3, 5:0.4, 6:0.5, 7:0.6, 8:0.7,9:0.8, 10:0.9, 11:1}
for dataset in train_test_data:
    dataset['FamilySize'] = dataset['FamilySize'].map(family_mapping)
    
col_drop = ['SibSp','Parch','Ticket', 'Name']
train_data = train_data.drop(col_drop, axis =1)
test_data = test_data.drop(col_drop, axis = 1)

train_features = ['Pclass','Age','Sex','Fare', 'Title']
test_features = ['Pclass','Age','Sex','Fare', 'Title']

train_X = train_data[train_features]
train_y = train_data.Survived

test_y = test_data[test_features]

model = XGBClassifier (learning_rate= 0.1)

model.fit(train_X,train_y)
prediction = model.predict(test_y)

Id = test_data['PassengerId']


my_submission = pd.DataFrame({'PassengerId':Id, 'Survived': prediction})
my_submission.to_csv('submission.csv', index=False)

print(my_submission)
