Titanic: A machine learning disaster

A prediction on the passengers that survived the Titanic disaster using Pandas and Seaborn chats, hosted by Kaggle.

A deep dive into the data set using techniques like data exploration & visualization, feature Engineering - understanding propotions in the data, bar charts, partial dependence plots and data stories that tell which features are likely to improve the accuracy of predictions.

Feature Engineering

Feature engineering is the process of using domain knowledge of the data to create features (feature vectors) that make machine learning algorithims work

feature vector is an n- dimensional vector of numerical features that represent some object.
Many algorithins in machine learning require a numerical representation of objects, since such representations facillitate processing and statistical analysis

The prediction is currently at 75% accuracy and I am working to improve that.